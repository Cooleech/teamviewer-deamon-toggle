#!/bin/bash
Pass=`zenity --entry --hide-text --text "Lozinka korisnika $(whoami)"`
systemctl --state=running | grep teamviewerd
if [ $? = 0 ]; then
 echo $Pass | sudo -S systemctl stop teamviewerd
 if [ $? = 0 ]; then
  zenity --info --text "Teamviewer deamon ugašen!" --timeout 2
 else
  zenity --error --text "Teamviewer deamon još radi!"
 fi
else
 echo $Pass | sudo -S systemctl start teamviewerd
 if [ $? = 0 ]; then
  teamviewer
 else
  zenity --error --text "Nastala je neka greška pri pokretanju Teamviewera!"
 fi
fi